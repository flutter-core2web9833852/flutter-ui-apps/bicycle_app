import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bicycle_app_v1/HomePage.dart';

class Cartpage extends StatefulWidget {
  const Cartpage({super.key});

  @override
  State createState() => _CartpageState();
}

class _CartpageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(36, 44, 59, 1),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 20.0, right: 20, top: 50, bottom: 20),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "My Shopping Cart",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 28,
                      fontWeight: FontWeight.w700),
                ),
                const Spacer(),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Homepage()));
                  },
                  child: Container(
                    height: 45,
                    width: 45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: const LinearGradient(colors: [
                          Color.fromARGB(255, 81, 174, 249),
                          Color.fromARGB(255, 8, 106, 186)
                        ])),
                    child: const Icon(
                      Icons.arrow_back_ios_new_rounded,
                      color: Colors.white,
                      size: 25,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(39, 82, 102, 139),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: const [
                        BoxShadow(
                            offset: Offset(3, 3),
                            color: Colors.black26,
                            blurRadius: 15,
                            spreadRadius: 1)
                      ],
                      border: Border.all(
                          width: 5,
                          color: const Color.fromARGB(174, 27, 34, 44),
                          strokeAlign: BorderSide.strokeAlignOutside)),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset(
                      "assets/images/bicycle2.png",
                      height: 90,
                      width: 120,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "GT Bike",
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 22,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 220,
                      child: Row(
                        children: [
                          Text(
                            "\$2,599.99",
                            style: GoogleFonts.poppins(
                                color: const Color.fromARGB(255, 8, 106, 186),
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                          const Spacer(),
                          // const SizedBox(
                          //   width: 50,
                          // ),
                          Container(
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 23, 28, 33),
                                borderRadius: BorderRadius.circular(10)),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: [
                                  Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                        color: const Color.fromARGB(
                                            255, 8, 106, 186),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: const Icon(
                                      Icons.add,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "1",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                      height: 25,
                                      width: 25,
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                              255, 26, 26, 26),
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: const Icon(
                                        Icons.exposure_minus_1,
                                        color: Colors.grey,
                                        size: 16,
                                      ))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
            const Divider(
              height: 50,
              thickness: 2,
              color: Color.fromARGB(157, 65, 79, 100),
            ),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(39, 82, 102, 139),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: const [
                        BoxShadow(
                            offset: Offset(3, 3),
                            color: Colors.black26,
                            blurRadius: 15,
                            spreadRadius: 1)
                      ],
                      border: Border.all(
                          width: 5,
                          color: const Color.fromARGB(174, 27, 34, 44),
                          strokeAlign: BorderSide.strokeAlignOutside)),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset(
                      "assets/images/helmet.png",
                      height: 90,
                      width: 120,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Helmet",
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 22,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 220,
                      child: Row(
                        children: [
                          Text(
                            "\$25.99",
                            style: GoogleFonts.poppins(
                                color: const Color.fromARGB(255, 8, 106, 186),
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                          const Spacer(),
                          // const SizedBox(
                          //   width: 50,
                          // ),
                          Container(
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 23, 28, 33),
                                borderRadius: BorderRadius.circular(10)),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: [
                                  Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                        color: const Color.fromARGB(
                                            255, 8, 106, 186),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: const Icon(
                                      Icons.add,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "1",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                      height: 25,
                                      width: 25,
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                              255, 26, 26, 26),
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: const Icon(
                                        Icons.exposure_minus_1,
                                        color: Colors.grey,
                                        size: 16,
                                      ))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
            const Divider(
              height: 50,
              thickness: 2,
              color: Color.fromARGB(157, 65, 79, 100),
            ),
            Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(39, 82, 102, 139),
                      borderRadius: BorderRadius.circular(18),
                      boxShadow: const [
                        BoxShadow(
                            offset: Offset(3, 3),
                            color: Colors.black26,
                            blurRadius: 15,
                            spreadRadius: 1)
                      ],
                      border: Border.all(
                          width: 5,
                          color: const Color.fromARGB(174, 27, 34, 44),
                          strokeAlign: BorderSide.strokeAlignOutside)),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Image.asset(
                      "assets/images/bottle.png",
                      height: 90,
                      width: 120,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Bottle",
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 22,
                          fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 220,
                      child: Row(
                        children: [
                          Text(
                            "\$15.99",
                            style: GoogleFonts.poppins(
                                color: const Color.fromARGB(255, 8, 106, 186),
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                          const Spacer(),
                          // const SizedBox(
                          //   width: 50,
                          // ),
                          Container(
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 23, 28, 33),
                                borderRadius: BorderRadius.circular(10)),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: [
                                  Container(
                                    height: 25,
                                    width: 25,
                                    decoration: BoxDecoration(
                                        color: const Color.fromARGB(
                                            255, 8, 106, 186),
                                        borderRadius: BorderRadius.circular(8)),
                                    child: const Icon(
                                      Icons.add,
                                      color: Colors.white,
                                      size: 18,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    "1",
                                    style: GoogleFonts.poppins(
                                        color: Colors.white),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                      height: 25,
                                      width: 25,
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                              255, 26, 26, 26),
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                      child: const Icon(
                                        Icons.exposure_minus_1,
                                        color: Colors.grey,
                                        size: 16,
                                      ))
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
            const Divider(
              height: 50,
              thickness: 2,
              color: Color.fromARGB(157, 65, 79, 100),
            ),
            Text(
              "Your Bag Qualifies for Free Shipping",
              style: GoogleFonts.poppins(
                  color: Colors.white60,
                  fontSize: 14,
                  fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              height: 50,
              decoration: BoxDecoration(
                  color: const Color.fromARGB(174, 27, 34, 44),
                  border: Border.all(
                    color: Color.fromARGB(173, 37, 49, 65),
                  ),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    "XF99r5",
                    style: GoogleFonts.poppins(
                        color: Colors.white60,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                  Spacer(),
                  Container(
                    height: 45,
                    width: 130,
                    decoration: BoxDecoration(
                        gradient: const LinearGradient(colors: [
                          Color.fromARGB(255, 81, 174, 249),
                          Color.fromARGB(255, 8, 106, 186)
                        ]),
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text(
                        textAlign: TextAlign.center,
                        "Apply",
                        style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: 22,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Text(
                  textAlign: TextAlign.center,
                  "Subtotal",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const Spacer(),
                Text(
                  "\$2,641.97",
                  style: GoogleFonts.poppins(
                      color: const Color.fromARGB(255, 8, 106, 186),
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  textAlign: TextAlign.center,
                  "Delivery fee",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const Spacer(),
                Text(
                  "\$0",
                  style: GoogleFonts.poppins(
                      color: const Color.fromARGB(255, 8, 106, 186),
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  textAlign: TextAlign.center,
                  "Discount",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const Spacer(),
                Text(
                  "\$20%",
                  style: GoogleFonts.poppins(
                      color: const Color.fromARGB(255, 8, 106, 186),
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Text(
                  textAlign: TextAlign.center,
                  "Total",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                const Spacer(),
                Text(
                  "\$2,510.00",
                  style: GoogleFonts.poppins(
                      color: const Color.fromARGB(255, 8, 106, 186),
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: const LinearGradient(colors: [
                        Color.fromARGB(255, 81, 174, 249),
                        Color.fromARGB(255, 8, 106, 186)
                      ])),
                  child: const Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: Colors.white,
                    size: 18,
                  ),
                ),
                const SizedBox(
                  width: 12,
                ),
                Text(
                  textAlign: TextAlign.center,
                  "Check Out",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
