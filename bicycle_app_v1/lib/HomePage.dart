import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bicycle_app_v1/DetailPage.dart';
import 'package:blurrycontainer/blurrycontainer.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State createState() => _HomepageState();
}

class _HomepageState extends State {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(36, 44, 59, 1),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 20.0, right: 20, top: 50, bottom: 20),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  "Choose Your \nBicycle",
                  style: GoogleFonts.poppins(
                      color: Colors.white,
                      fontSize: 28,
                      fontWeight: FontWeight.w700),
                ),
                const Spacer(),
                Container(
                  height: 45,
                  width: 45,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: const LinearGradient(colors: [
                        Color.fromARGB(255, 81, 174, 249),
                        Color.fromARGB(255, 8, 106, 186)
                      ])),
                  child: Image.asset("assets/icon/search-normal.png"),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Stack(
              children: [
                Positioned(
                    right: 10,
                    child: Image.asset(
                      "assets/icon/EXTREME.png",
                      height: 500,
                      color: Colors.white38,
                    )),
                Container(
                  height: 633,
                  child: ListView(children: [
                    BlurryContainer(
                      height: 250,
                      width: 380,
                      blur: 5,
                      elevation: 1,
                      color: Color.fromARGB(104, 82, 102, 139),
                      padding: const EdgeInsets.all(8),
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      // decoration: BoxDecoration(
                      //     color: Color.fromARGB(104, 82, 102, 139),
                      //     borderRadius: BorderRadius.circular(20),
                      //     boxShadow: const [
                      //       BoxShadow(
                      //           offset: Offset(15, 15),
                      //           color: Colors.black26,
                      //           blurRadius: 20)
                      //     ],
                      //     border: Border.all(
                      //       width: 3,
                      //       color: const Color.fromARGB(162, 42, 49, 62),
                      //     )),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          children: [
                            Image.asset("assets/images/bicycle6.png"),
                            Column(
                              children: [
                                Text(
                                  "Extreme",
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                                const Spacer(),
                                Text(
                                  "30% OFF",
                                  style: GoogleFonts.poppins(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              gradient: const LinearGradient(colors: [
                                Color.fromARGB(255, 81, 174, 249),
                                Color.fromARGB(255, 8, 106, 186)
                              ])),
                          child: Image.asset(
                            "assets/icon/bcycle.png",
                            scale: 1.3,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Image.asset(
                            "assets/icon/gloves.png",
                            scale: 1.3,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          child: Image.asset(
                            "assets/icon/helmet.png",
                            scale: 1.3,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Container(
                          height: 50,
                          width: 50,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white,
                          ),
                          child: Image.asset(
                            "assets/icon/bottle.png",
                            scale: 1.3,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    GridView.custom(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(
                            parent: ScrollPhysics()),

                        // maxCrossAxisExtent: 250,
                        // mainAxisSpacing: 20,
                        // crossAxisSpacing: 20,
                        // gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        //     crossAxisCount: 4,
                        //     mainAxisSpacing: 20,
                        //     crossAxisSpacing: 20,
                        //     mainAxisExtent: 200,
                        //     childAspectRatio: 3),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisExtent: 250,
                                mainAxisSpacing: 20,
                                crossAxisSpacing: 20,
                                childAspectRatio: 2),
                        childrenDelegate: SliverChildListDelegate.fixed(
                          [
                            // children: [
                            BlurryContainer(
                              blur: 5,
                              elevation: 1,
                              color: Color.fromARGB(104, 82, 102, 139),
                              padding: const EdgeInsets.all(8),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),

                              // child: Text("hello"),
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      "assets/images/bicycle11.png",
                                      height: 132,
                                    ),
                                    Text(
                                      "Road Bike",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      "Kiross",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    // const Spacer(),
                                    Text(
                                      "\$1,599.99",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const Detailpage()));
                              },
                              child: BlurryContainer(
                                blur: 5,
                                elevation: 1,
                                color: Color.fromARGB(104, 82, 102, 139),
                                padding: const EdgeInsets.all(8),
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),

                                // child: Text("hello"),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                        "assets/images/bicycle2.png",
                                        height: 136,
                                      ),
                                      Text(
                                        "Road Bike",
                                        style: GoogleFonts.poppins(
                                            color: Colors.white38,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Text(
                                        "GT Bike",
                                        style: GoogleFonts.poppins(
                                            color: Colors.white,
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      // const Spacer(),
                                      Text(
                                        "\$2,599.99",
                                        style: GoogleFonts.poppins(
                                            color: Colors.white38,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            BlurryContainer(
                              blur: 5,
                              elevation: 1,
                              color: Color.fromARGB(104, 82, 102, 139),
                              padding: const EdgeInsets.all(8),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),

                              // child: Text("hello"),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      "assets/images/bicycle7.png",
                                      height: 136,
                                    ),
                                    Text(
                                      "Road Bike",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      "Atlas",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    // const Spacer(),
                                    Text(
                                      "\$8,99.99",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            BlurryContainer(
                              blur: 5,
                              elevation: 1,
                              color: Color.fromARGB(104, 82, 102, 139),
                              padding: const EdgeInsets.all(8),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),

                              // child: Text("hello"),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      "assets/images/bicycle8.png",
                                      height: 136,
                                    ),
                                    Text(
                                      "Road Bike",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      "BlackFox",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white,
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    // const Spacer(),
                                    Text(
                                      "\$3,199.99",
                                      style: GoogleFonts.poppins(
                                          color: Colors.white38,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ))
                  ]),
                )
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(162, 42, 49, 62),
        height: 80,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations: const [
          NavigationDestination(
              icon: Icon(
                Icons.home_rounded,
                size: 30,
                color: Colors.white,
              ),
              selectedIcon: Icon(Icons.home_rounded),
              label: ""),
          NavigationDestination(
            icon: Icon(
              Icons.shopping_bag_outlined,
              size: 30,
              color: Colors.white,
            ),
            label: "",
          ),
          NavigationDestination(
            icon: Icon(
              Icons.wallet_rounded,
              size: 30,
              color: Colors.white,
            ),
            label: '',
          ),
          NavigationDestination(
              icon: Icon(
                Icons.account_circle_outlined,
                size: 30,
                color: Colors.white,
              ),
              label: ""),
        ],
      ),
    );
  }
}
