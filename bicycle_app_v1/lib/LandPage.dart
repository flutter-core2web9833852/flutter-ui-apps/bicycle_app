import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:bicycle_app_v1/HomePage.dart';

class Landpage extends StatefulWidget {
  const Landpage({super.key});

  @override
  State createState() => _LandpageState();
}

class _LandpageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(36, 44, 59, 1),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const SizedBox(
            height: 50,
          ),
          const Image(
            image: AssetImage("assets/icon/cycle.png"),
            alignment: Alignment.center,
          ),
          const SizedBox(
            height: 100,
          ),
          Stack(
            children: [
              // Positioned(
              //     bottom: 0,
              //     // ignore: sized_box_for_whitespace
              //     child: Container(
              //       height: 500,
              //       width: 500,
              //       //color: Colors.white,
              //       child: CustomPaint(
              //         painter: CurvePainter(),
              //       ),
              //     )),
              Positioned(
                  right: 10,
                  child: Image.asset(
                    "assets/icon/EXTREME.png",
                    height: 500,
                    color: Colors.white38,
                  )),
              Positioned(child: Image.asset("assets/images/pngwing.png"))
            ],
          ),
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Homepage()));
                },
                child: Container(
                  height: 75,
                  width: 240,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(36, 44, 59, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          bottomLeft: Radius.circular(50)),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0.5, -2),
                          blurRadius: 25,
                          color: Color.fromRGBO(41, 57, 86, 1),
                        )
                      ]),
                  child: Row(
                    children: [
                      Container(
                        height: 63,
                        width: 63,
                        decoration: const BoxDecoration(
                            color: Color.fromRGBO(75, 76, 237, 1),
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0.5, -0.1),
                                  spreadRadius: 2,
                                  color: Color.fromRGBO(239, 186, 51, 1),
                                  blurRadius: 4)
                            ]),
                        child: const Icon(
                          Icons.arrow_forward_ios_rounded,
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        "Get Start",
                        style: GoogleFonts.poppins(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        width: 50,
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

class CurvePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint();
    paint.color = Colors.green;
    paint.style = PaintingStyle.fill;

    var path = Path();

    path.moveTo(0, size.height * 0.9167);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.875,
        size.width * 0.5, size.height * 0.9167);
    path.quadraticBezierTo(size.width * 0.75, size.height * 0.9584,
        size.width * 1.0, size.height * 0.9167);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
