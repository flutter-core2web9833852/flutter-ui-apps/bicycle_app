import 'package:bicycle_app_v1/CartPage.dart';
import 'package:bicycle_app_v1/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:blurrycontainer/blurrycontainer.dart';

class Detailpage extends StatefulWidget {
  const Detailpage({super.key});

  @override
  State createState() => _DetailpageState();
}

class _DetailpageState extends State {
  final _imgController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(36, 44, 59, 1),
      body: Padding(
        padding:
            const EdgeInsets.only(left: 0.0, right: 0, top: 50, bottom: 20),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Row(
                children: [
                  Text(
                    "GT Bike",
                    style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 28,
                        fontWeight: FontWeight.w700),
                  ),
                  const Spacer(),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Homepage()));
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          gradient: const LinearGradient(colors: [
                            Color.fromARGB(255, 81, 174, 249),
                            Color.fromARGB(255, 8, 106, 186)
                          ])),
                      child: const Icon(
                        Icons.arrow_back_ios_new_rounded,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Expanded(
                child: PageView(controller: _imgController, children: [
              Image.asset("assets/images/bicycle2.png"),
              Image.asset("assets/images/bicycle3.png"),
              Image.asset("assets/images/bicycle4.png"),
            ])),
            SmoothPageIndicator(
              count: 3,
              controller: _imgController,
              effect: const WormEffect(
                dotHeight: 12,
                dotWidth: 12,
                dotColor: Colors.white,
                activeDotColor: Color.fromARGB(255, 18, 27, 44),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            BlurryContainer(
              blur: 5,
              elevation: 1,
              color: Color.fromARGB(104, 82, 102, 139),
              padding: const EdgeInsets.all(8),
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: const Color.fromRGBO(36, 44, 59, 1),
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                width: 3,
                                color: Color.fromARGB(186, 113, 139, 186),
                              )),
                          child: Row(
                            children: [
                              Column(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8),
                                    child: Text(
                                      "Description",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Container(
                          decoration: BoxDecoration(
                              gradient: const LinearGradient(colors: [
                                Color.fromARGB(255, 81, 174, 249),
                                Color.fromARGB(255, 8, 106, 186)
                              ]),
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                width: 3,
                                color: Color.fromARGB(186, 113, 139, 186),
                              )),
                          child: Row(
                            children: [
                              Column(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8),
                                    child: Text(
                                      "Specification",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.poppins(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 145,
                      child: ListView(
                        shrinkWrap: false,
                        children: [
                          Text(
                            "Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(162, 42, 49, 62),
        height: 100,
        destinations: [
          Padding(
            padding: const EdgeInsets.only(left: 30.0),
            child: Text(
              "\$2,599.99",
              style: GoogleFonts.poppins(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.w600),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Cartpage()));
              },
              child: Container(
                height: 50,
                width: 100,
                decoration: BoxDecoration(
                    gradient: const LinearGradient(colors: [
                      Color.fromARGB(255, 81, 174, 249),
                      Color.fromARGB(255, 8, 106, 186)
                    ]),
                    borderRadius: BorderRadius.circular(18)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Buy Now",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
